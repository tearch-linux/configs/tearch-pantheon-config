DESTDIR=/

all: install

install:
	mkdir -p $(DESTDIR)/etc/xdg/ || true
	mkdir -p $(DESTDIR)/etc/skel/.config/ || true
	mkdir -p $(DESTDIR)/usr/share/glib-2.0/schemas/ || true
	
	cp -rfv xdg/* $(DESTDIR)/etc/xdg/
	cp -rfv skel/* $(DESTDIR)/etc/skel/.config/
	cp -rfv schemas/* $(DESTDIR)/usr/share/glib-2.0/schemas/